﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfBrowserApp1.Common;

namespace WpfBrowserApp1.View
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Page1 : Page, IMainWindow
    {
        private Presenter.Page1Presenter Presenter { get; set; }

        public Page1(IMainWindow viewMain)
        {
            this.InitializeComponent();
            this.Presenter = new Presenter.Page1Presenter(this, viewMain);
        }
    }
}
