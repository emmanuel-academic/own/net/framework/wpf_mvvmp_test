﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfBrowserApp1.Model
{
    public class MainWindowModel : INotifyPropertyChanged
    {

       private string _message;

        public string Message
        {
            get { return this._message; }
            set
            {
                this._message = value;
                this.PropertyChanged(this, new PropertyChangedEventArgs("Message"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

    }
}
