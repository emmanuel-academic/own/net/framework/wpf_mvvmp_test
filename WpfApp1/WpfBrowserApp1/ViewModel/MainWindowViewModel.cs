﻿using System.Windows.Input;
using WpfBrowserApp1.Model;

namespace WpfBrowserApp1.ViewModel
{
    public class MainWindowViewModel
    {
        public object KeepAlive { get; set; }
        public ICommand ClickCommand { get; set; }
        public MainWindowModel Model { get; set; } = new MainWindowModel();
    }
}
