﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfBrowserApp1.Model;

namespace WpfBrowserApp1.ViewModel
{
    public class Page1ViewModel
    {
        public object KeepAlive { get; set; }
        public ICommand ClickCommand { get; set; }
        public Page1Model Model { get; set; } = new Page1Model();
    }
}
