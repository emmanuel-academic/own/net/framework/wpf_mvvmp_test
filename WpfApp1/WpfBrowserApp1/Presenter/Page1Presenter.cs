﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfBrowserApp1.Common;
using WpfBrowserApp1.ViewModel;

namespace WpfBrowserApp1.Presenter
{
    public class Page1Presenter
    {

        readonly Page1ViewModel _viewModel;

        public IMainWindow View { get; private set; }

        public Page1Presenter(IMainWindow view, IMainWindow Mainview)
        {
            View = view;
            _viewModel = new Page1ViewModel
            {
                KeepAlive = this,
                ClickCommand = new DelegateCommand(ClickCommand_Execute, _ => true)
            };

            View.DataContext = _viewModel;
        }

        void ClickCommand_Execute(object args)
        {
            _viewModel.Model.Message = "Hello, World";
        }
    }
}
