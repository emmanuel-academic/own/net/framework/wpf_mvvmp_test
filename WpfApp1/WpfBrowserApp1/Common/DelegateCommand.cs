﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace WpfBrowserApp1.Common
{
    class DelegateCommand : ICommand
    {
        readonly Action<object> _onExecute;
        readonly Func<object, bool> _canExecute;

        public event EventHandler CanExecuteChanged = delegate { };

        public DelegateCommand(Action<object> onExecute, Func<object, bool> canExecute)
        {
            this._onExecute = onExecute;
            this._canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this._canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this._onExecute(parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}
